package frangPolloApp.model.productos.managers;



import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import frangPolloApp.model.core.entities.ProdCategoria;
import frangPolloApp.model.core.entities.ProdProducto;

/**
 * Session Bean implementation class ManagerProductos
 */
@Stateless
@LocalBean
public class ManagerProductos {
	@PersistenceContext
	private EntityManager em;
 
    public ManagerProductos() {
    }
   
    public List<ProdProducto> findAllProdProducto(){
    	return em.createNamedQuery("ProdProducto.findAll",ProdProducto.class).getResultList();
    }
    public List<ProdCategoria> findAllProdCategoria(){
    	return em.createNamedQuery("ProdCategoria.findAll",ProdCategoria.class).getResultList();
    }
    public ProdProducto findProdById(int id) {
    	return em.find(ProdProducto.class, id);
    }
    public ProdCategoria findCatById(int cat) {
    	return em.find(ProdCategoria.class, cat);
    }
    public void insertarCategoria(ProdCategoria categoria) {
    	em.persist(categoria);
    	em.clear();
    }
    
    public void eliminarCategoria(int idcat) {
    	ProdCategoria categoria=findCatById(idcat);
    	if(categoria!=null)
    		em.remove(categoria);
    }
   
    public void actualizarCategoria(ProdCategoria categoria) {
    	ProdCategoria cat=findCatById(categoria.getIdCatPro());
    	cat.setNombreCatPro(categoria.getNombreCatPro());
    	cat.setDetalleCatPro(categoria.getDetalleCatPro());
    
    	em.merge(cat);
    }
    
    public void insertarProducto(ProdProducto producto) {
    	
    	em.persist(producto);

    }
    
    public void eliminarProducto(int id) {
    	ProdProducto producto=findProdById(id);
    	if(producto!=null)
    		em.remove(producto);
    }
   
    public void actualizarProducto(ProdProducto producto) {
    	ProdProducto prod=findProdById(producto.getIdProd());
    	prod.setNombreProd(producto.getNombreProd());
    	prod.setDetalleProd(producto.getDetalleProd());
    	prod.setPrecioProd(producto.getPrecioProd());
    	prod.setProdCategoria(producto.getProdCategoria());
    	prod.setImgProd(producto.getImgProd());
    
    	em.merge(prod);
    }
}
