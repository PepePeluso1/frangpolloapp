package frangPolloApp.model.cliente.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import frangPolloApp.model.core.entities.CliCliente;

/**
 * Session Bean implementation class ManagerCliente
 */
@Stateless
@LocalBean
public class ManagerCliente {

	@PersistenceContext
    private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public ManagerCliente() {
        // TODO Auto-generated constructor stub
    }
    
    public List<CliCliente> findAllCliClientes(){
    	return em.createNamedQuery("CliCliente.findAll",CliCliente.class).getResultList();
    }
    
    public CliCliente findClienteByCedula(String cedula) {
    	return em.find(CliCliente.class,cedula);
    }
    
    public void insertarCliCliente(CliCliente cliente) throws Exception {
    	if(!(findClienteByCedula(cliente.getCedulaCliente())==null)) {
    		throw new Exception("Ya existe la c�dula indicada.");
    	} else {
    		em.persist(cliente);
    	}
    }
    
    public void eliminarCliCliente(String cedula) {
    	CliCliente cliente = findClienteByCedula(cedula);
		if (!(cliente==null)) {
			em.remove(cliente);
		}
	}
	
	public void actualizarCliCliente(CliCliente cliCliente) throws Exception {
		CliCliente cliente = findClienteByCedula(cliCliente.getCedulaCliente());
		if (cliente==null)
			throw new Exception("No existe el Cliente con la c�dula indicada.");
		else {
			cliente.setNombresCliente(cliCliente.getNombresCliente());
			cliente.setApellidosCliente(cliCliente.getApellidosCliente());
			cliente.setDireccionCliente(cliCliente.getDireccionCliente());
			cliente.setTelefonoCliente(cliCliente.getTelefonoCliente());
			cliente.setCorreoCliente(cliCliente.getCorreoCliente());
			em.merge(cliente);
		}
	}

}
