package frangPolloApp.model.inventario.managers;

import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import frangPolloApp.model.auditoria.managers.ManagerAuditoria;
import frangPolloApp.model.core.entities.InvCategoria;
import frangPolloApp.model.core.entities.InvInventario;
import frangPolloApp.model.core.managers.ManagerDAO;

/**
 * Session Bean implementation class ManagerInventario
 */
@Stateless
@LocalBean
public class ManagerInventario {

	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;
	@PersistenceContext
    private EntityManager em;
    public ManagerInventario() {
    
    }
    
    public List<InvInventario> findAllInvInventario(){
    	return mDAO.findAll(InvInventario.class,"nombreInv");
    }
        
    public InvInventario insertarInventario(InvInventario nuevoInventario, int idcategoria) throws Exception {
    	InvInventario inventario=new InvInventario();
    	InvCategoria categoria = (InvCategoria)mDAO.findById(InvCategoria.class, idcategoria);
    	inventario.setNombreInv(nuevoInventario.getNombreInv());
    	inventario.setInvCategoria(categoria);
    	inventario.setCantActualInv(nuevoInventario.getCantActualInv());
    	inventario.setCantMinimaInv(nuevoInventario.getCantMinimaInv());
    	mDAO.insertar(inventario);
    	return inventario;
    }
    
    
    public void eliminarInventario(int idInv) throws Exception {
    	mDAO.eliminar(InvInventario.class, idInv);
    }
    
    public void actualizarInventario(InvInventario edicionInventario, int idcategoria) throws Exception {
    	InvInventario inventario=(InvInventario) mDAO.findById(InvInventario.class, edicionInventario.getIdInv());
    	InvCategoria categoria = (InvCategoria)mDAO.findById(InvCategoria.class, idcategoria);
    	inventario.setNombreInv(edicionInventario.getNombreInv());
    	inventario.setInvCategoria(categoria);
    	inventario.setCantActualInv(edicionInventario.getCantActualInv());
    	inventario.setCantMinimaInv(edicionInventario.getCantMinimaInv());
    	mDAO.actualizar(inventario);
    }
    public void actualizarInventarioCantidad(InvInventario aProducto, int adicionProducto) throws Exception {
    	InvInventario inventario=(InvInventario) mDAO.findById(InvInventario.class, aProducto.getIdInv());
    	inventario.setCantActualInv(aProducto.getCantActualInv()+adicionProducto);
    	mDAO.actualizar(inventario);
    }
    
    public List<InvInventario> findMinStockInvInventario(){
    	String consulta="o.cantActualInv <= o.cantMinimaInv";
    	List<InvInventario> listStock = mDAO.findWhere(InvInventario.class, consulta, null);
    	return listStock;
    }

}
