package frangPolloApp.model.inventario.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import frangPolloApp.model.auditoria.managers.ManagerAuditoria;
import frangPolloApp.model.core.entities.InvCategoria;
import frangPolloApp.model.core.managers.ManagerDAO;
import frangPolloApp.model.seguridades.dtos.LoginDTO;

/**
 * Session Bean implementation class ManagerCategoriaInv
 */
@Stateless
@LocalBean
public class ManagerCategoriaInv {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;
	@PersistenceContext
    private EntityManager em;
	
    public ManagerCategoriaInv() {
        // TODO Auto-generated constructor stub
    }
    
    public List<InvCategoria> findAllInvCategoria() {
    	return mDAO.findAll(InvCategoria.class, "nombreCat");
    }
    
    public InvCategoria insertarCategoria(InvCategoria nuevaCategoria) throws Exception{
    	InvCategoria categoria = new InvCategoria();
    	categoria.setNombreCat(nuevaCategoria.getNombreCat());
    	categoria.setDetalleCat(nuevaCategoria.getDetalleCat());
    	mDAO.insertar(categoria);
    	return categoria;
    }
    
    public void actualizarCategoria(LoginDTO loginDTO,InvCategoria edicionCategoria) throws Exception {
    	InvCategoria categoria=(InvCategoria) mDAO.findById(InvCategoria.class, edicionCategoria.getIdCat());
    	categoria.setNombreCat(edicionCategoria.getNombreCat());
    	categoria.setDetalleCat(edicionCategoria.getDetalleCat());
    	mDAO.actualizar(categoria);
    	mAuditoria.mostrarLog(loginDTO, getClass(), "actualizarCategoria", "se actualizó la categoría "+categoria.getNombreCat());
    }
    
    public void eliminarCategoria(int idCat) throws Exception {
    	InvCategoria categoria=(InvCategoria) mDAO.findById(InvCategoria.class, idCat);
    	if(categoria.getInvInventarios().size()>0)
    		throw new Exception("No se puede eliminar la categoria porque tiene productos en el inventario");
    	mDAO.eliminar(InvCategoria.class, categoria.getIdCat());
    }

}
