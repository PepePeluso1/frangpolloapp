package frangPolloApp.model.factura.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import frangPolloApp.model.core.entities.FacCabecera;
import frangPolloApp.model.core.entities.FacDetalle;
import frangPolloApp.model.core.managers.ManagerDAO;
import frangPolloApp.model.core.entities.CliCliente;
import frangPolloApp.model.core.entities.ProdProducto;

/**
 * Session Bean implementation class ManagerFacturaa
 */
@Stateless
@LocalBean
public class ManagerFactura {

	public final static double PORCENTAJE_IVA=12;
	@PersistenceContext
	private EntityManager em;
	
	
	private FacCabecera FCabecera;
	private FacDetalle FDetalle;
	private Timestamp fecha;
	private LocalDateTime stamp;
	
	@EJB
	private ManagerDAO mDAO;
	
    public ManagerFactura() {
    	
    }

    
    public List<FacCabecera> findAllFacCabecera(){
    	return em.createNamedQuery("FacCabecera.findAll", FacCabecera.class).getResultList();
    }
    
    public List<ProdProducto> findAllProdProducto(){
    	return em.createNamedQuery("ProdProducto.findAll", ProdProducto.class).getResultList();
    }
    
    public FacCabecera generarFactura(String cedula, List<FacDetalle> carrito) {
    	CliCliente c = em.find(CliCliente.class, cedula);
    	stamp = LocalDateTime.now();
    	
    	FCabecera = new FacCabecera();
    	System.out.println(generarNroFactura());
    	FCabecera.setNroFactura(generarNroFactura());
    	FCabecera.setFechaFactura(Timestamp.valueOf(stamp));
    	FCabecera.setCliCliente(c);
    	FCabecera.setSubtotalFactura(calcularSubtotalFactura(carrito));
    	FCabecera.setPorcentajeIvaFactura(PORCENTAJE_IVA);
    	FCabecera.setValorIvaFactura((PORCENTAJE_IVA/100)*FCabecera.getSubtotalFactura());
    	FCabecera.setTotalFactura(FCabecera.getSubtotalFactura()+FCabecera.getValorIvaFactura());
    	em.persist(FCabecera);
    	asignarNroFacDetalle(carrito, FCabecera);
    	insertarCarrito(carrito);
    	
    	return FCabecera;
    }
    
    public void insertarCabeceera() {
    	
    }
    
    public void eliminarCabecera(String nroFactura) {
    	FacCabecera cabecera = findCabeceraByNro(nroFactura);
    	if(cabecera!=null)
    		em.remove(cabecera);
    }
    
    public void actualizarCabecera(FacCabecera cabecera) throws Exception {
    	FacCabecera c = findCabeceraByNro(cabecera.getNroFactura());
    	if(c==null)
    		throw new Exception("No existe el estudiante con la cedula especificada");
    	c.setCliCliente(cabecera.getCliCliente());
    	c.setFechaFactura(cabecera.getFechaFactura());
    	c.setPorcentajeIvaFactura(cabecera.getPorcentajeIvaFactura());
    	c.setSubtotalFactura(cabecera.getSubtotalFactura());
    	c.setTotalFactura(cabecera.getTotalFactura());
    	c.setValorIvaFactura(cabecera.getValorIvaFactura());
    	em.merge(c);
    }

    public void asignarNroFacDetalle(List<FacDetalle> carrito, FacCabecera f){

    	for(FacDetalle detalle:carrito) {
    		detalle.setFacCabecera(f);
    	}
    	
    }
    
    
    public double calcularSubtotalFactura(List<FacDetalle> carrito){
    	double subtotal = 0;
    	for(FacDetalle detalle:carrito) {
    		subtotal = subtotal + detalle.getTotalDetalleProd();
    	}
    	return subtotal;
    }
    
 
    
    
    public void insertarCarrito(List<FacDetalle> carrito){
    	for(FacDetalle detalle:carrito) {
    		em.persist(detalle);
    	}
    }
    
    public String generarNroFactura() {
    	List<FacCabecera> lista = findAllFacCabecera();
    	FacCabecera ultimaFacCabecera = lista.get(lista.size()-1);
    	BigInteger ultimoNroFactura = new BigInteger(ultimaFacCabecera.getNroFactura());
    	System.out.println("ultimo:"+ultimoNroFactura);
    	BigInteger uno = new BigInteger("1");
    	System.out.println("siguiente:"+ultimoNroFactura.add(uno));
    	return "00"+ultimoNroFactura.add(uno); 
    }
    
    public String ultimoNroFactura() {
    	List<FacCabecera> lista = findAllFacCabecera();
    	FacCabecera ultimaFacCabecera = lista.get(lista.size()-1);
    	BigInteger ultimoNroFactura = new BigInteger(ultimaFacCabecera.getNroFactura());
    	BigInteger uno = new BigInteger("1");
    	return "00"+ultimoNroFactura.add(uno); 
    }
    
    
    
    public List<FacDetalle> findAllFacDetalle(){
    	return em.createNamedQuery("FacDetalle.findAll", FacDetalle.class).getResultList();
    }
    
    public List<CliCliente> findAllCliCliente(){
    	return em.createNamedQuery("CliCliente.findAll", CliCliente.class).getResultList();
    }
    
    public List<FacDetalle> findAllFacDetalleByNroFActura(String NroFactura){
    	return (List<FacDetalle>) mDAO.findWhere(FacDetalle.class, "nro_fact='"+NroFactura+"'", null);
    }
    
    public ProdProducto findProductoById(int id) {
    	return em.find(ProdProducto.class, id);
    }
    
    public FacCabecera findCabeceraByNro(String nroFactura) {
    	return em.find(FacCabecera.class, nroFactura);
    } 
    
    public CliCliente findClienteBycedula(String cedula) {
    	return em.find(CliCliente.class, cedula);
    } 

}
