package frangPolloApp.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cli_usuario database table.
 * 
 */
@Entity
@Table(name="cli_usuario")
@NamedQuery(name="CliUsuario.findAll", query="SELECT c FROM CliUsuario c")
public class CliUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usuario", unique=true, nullable=false)
	private Integer idUsuario;

	@Column(name="clave_usuario", nullable=false, length=2147483647)
	private String claveUsuario;

	//bi-directional many-to-one association to CliCliente
	@ManyToOne(cascade = {CascadeType.MERGE})
	@JoinColumn(name="cedula_cliente", nullable=false)
	private CliCliente cliCliente;

	public CliUsuario() {
	}

	public Integer getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getClaveUsuario() {
		return this.claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public CliCliente getCliCliente() {
		return this.cliCliente;
	}

	public void setCliCliente(CliCliente cliCliente) {
		this.cliCliente = cliCliente;
	}

}