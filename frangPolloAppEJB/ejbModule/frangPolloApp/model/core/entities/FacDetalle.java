package frangPolloApp.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the fac_detalle database table.
 * 
 */
@Entity
@Table(name="fac_detalle")
@NamedQuery(name="FacDetalle.findAll", query="SELECT f FROM FacDetalle f")
public class FacDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_fac", unique=true, nullable=false)
	private Integer idDetalleFac;

	@Column(name="cantidad_prod", nullable=false)
	private Integer cantidadProd;

	@Column(name="precio_unit_prod", nullable=false)
	private double precioUnitProd;

	@Column(name="total_detalle_prod", nullable=false)
	private double totalDetalleProd;

	//bi-directional many-to-one association to FacCabecera
	@ManyToOne
	@JoinColumn(name="nro_fact", nullable=false)
	private FacCabecera facCabecera;

	
	//bi-directional many-to-one association to ProdProducto
	@ManyToOne
	@JoinColumn(name="id_prod", nullable=false)
	private ProdProducto prodProducto;

	public FacDetalle() {
	}

	public Integer getIdDetalleFac() {
		return this.idDetalleFac;
	}

	public void setIdDetalleFac(Integer idDetalleFac) {
		this.idDetalleFac = idDetalleFac;
	}

	public Integer getCantidadProd() {
		return this.cantidadProd;
	}

	public void setCantidadProd(Integer cantidadProd) {
		this.cantidadProd = cantidadProd;
	}

	public double getPrecioUnitProd() {
		return this.precioUnitProd;
	}

	public void setPrecioUnitProd(double precioUnitProd) {
		this.precioUnitProd = precioUnitProd;
	}

	public double getTotalDetalleProd() {
		return this.totalDetalleProd;
	}

	public void setTotalDetalleProd(double totalDetalleProd) {
		this.totalDetalleProd = totalDetalleProd;
	}

	public FacCabecera getFacCabecera() {
		return this.facCabecera;
	}

	public void setFacCabecera(FacCabecera facCabecera) {
		this.facCabecera = facCabecera;
	}

	public ProdProducto getProdProducto() {
		return this.prodProducto;
	}

	public void setProdProducto(ProdProducto prodProducto) {
		this.prodProducto = prodProducto;
	}

}