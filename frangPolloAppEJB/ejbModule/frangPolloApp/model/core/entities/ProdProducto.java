package frangPolloApp.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the prod_producto database table.
 * 
 */
@Entity
@Table(name="prod_producto")
@NamedQuery(name="ProdProducto.findAll", query="SELECT p FROM ProdProducto p")
public class ProdProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_prod", unique=true, nullable=false)
	private Integer idProd;

	@Column(name="detalle_prod", nullable=false, length=2147483647)
	private String detalleProd;

	@Column(name="img_prod", length=2147483647)
	private String imgProd;

	@Column(name="nombre_prod", nullable=false, length=50)
	private String nombreProd;

	@Column(name="precio_prod", nullable=false)
	private double precioProd;

	//bi-directional many-to-one association to FacDetalle
	@OneToMany(mappedBy="prodProducto")
	private List<FacDetalle> facDetalles;

	//bi-directional many-to-one association to ProdCategoria
	@ManyToOne
	@JoinColumn(name="id_cat_prod", nullable=false)
	private ProdCategoria prodCategoria;

	public ProdProducto() {
	}

	public Integer getIdProd() {
		return this.idProd;
	}

	public void setIdProd(Integer idProd) {
		this.idProd = idProd;
	}

	public String getDetalleProd() {
		return this.detalleProd;
	}

	public void setDetalleProd(String detalleProd) {
		this.detalleProd = detalleProd;
	}

	public String getImgProd() {
		return this.imgProd;
	}

	public void setImgProd(String imgProd) {
		this.imgProd = imgProd;
	}

	public String getNombreProd() {
		return this.nombreProd;
	}

	public void setNombreProd(String nombreProd) {
		this.nombreProd = nombreProd;
	}

	public double getPrecioProd() {
		return this.precioProd;
	}

	public void setPrecioProd(double precioProd) {
		this.precioProd = precioProd;
	}

	public List<FacDetalle> getFacDetalles() {
		return this.facDetalles;
	}

	public void setFacDetalles(List<FacDetalle> facDetalles) {
		this.facDetalles = facDetalles;
	}

	public FacDetalle addFacDetalle(FacDetalle facDetalle) {
		getFacDetalles().add(facDetalle);
		facDetalle.setProdProducto(this);

		return facDetalle;
	}

	public FacDetalle removeFacDetalle(FacDetalle facDetalle) {
		getFacDetalles().remove(facDetalle);
		facDetalle.setProdProducto(null);

		return facDetalle;
	}

	public ProdCategoria getProdCategoria() {
		return this.prodCategoria;
	}

	public void setProdCategoria(ProdCategoria prodCategoria) {
		this.prodCategoria = prodCategoria;
	}

}