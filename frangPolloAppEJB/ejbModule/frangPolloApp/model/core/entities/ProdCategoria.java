package frangPolloApp.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the prod_categoria database table.
 * 
 */
@Entity
@Table(name="prod_categoria")
@NamedQuery(name="ProdCategoria.findAll", query="SELECT p FROM ProdCategoria p")
public class ProdCategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cat_pro", unique=true, nullable=false)
	private Integer idCatPro;

	@Column(name="detalle_cat_pro", nullable=false, length=50)
	private String detalleCatPro;

	@Column(name="nombre_cat_pro", nullable=false, length=50)
	private String nombreCatPro;

	//bi-directional many-to-one association to ProdProducto
	@OneToMany(mappedBy="prodCategoria")
	private List<ProdProducto> prodProductos;

	public ProdCategoria() {
	}

	public Integer getIdCatPro() {
		return this.idCatPro;
	}

	public void setIdCatPro(Integer idCatPro) {
		this.idCatPro = idCatPro;
	}

	public String getDetalleCatPro() {
		return this.detalleCatPro;
	}

	public void setDetalleCatPro(String detalleCatPro) {
		this.detalleCatPro = detalleCatPro;
	}

	public String getNombreCatPro() {
		return this.nombreCatPro;
	}

	public void setNombreCatPro(String nombreCatPro) {
		this.nombreCatPro = nombreCatPro;
	}

	public List<ProdProducto> getProdProductos() {
		return this.prodProductos;
	}

	public void setProdProductos(List<ProdProducto> prodProductos) {
		this.prodProductos = prodProductos;
	}

	public ProdProducto addProdProducto(ProdProducto prodProducto) {
		getProdProductos().add(prodProducto);
		prodProducto.setProdCategoria(this);

		return prodProducto;
	}

	public ProdProducto removeProdProducto(ProdProducto prodProducto) {
		getProdProductos().remove(prodProducto);
		prodProducto.setProdCategoria(null);

		return prodProducto;
	}

}