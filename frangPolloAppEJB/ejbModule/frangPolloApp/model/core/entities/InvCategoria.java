package frangPolloApp.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the inv_categoria database table.
 * 
 */
@Entity
@Table(name="inv_categoria")
@NamedQuery(name="InvCategoria.findAll", query="SELECT i FROM InvCategoria i")
public class InvCategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cat", unique=true, nullable=false)
	private Integer idCat;

	@Column(name="detalle_cat", nullable=false, length=2147483647)
	private String detalleCat;

	@Column(name="nombre_cat", nullable=false, length=50)
	private String nombreCat;

	//bi-directional many-to-one association to InvInventario
	@OneToMany(mappedBy="invCategoria")
	private List<InvInventario> invInventarios;

	public InvCategoria() {
	}

	public Integer getIdCat() {
		return this.idCat;
	}

	public void setIdCat(Integer idCat) {
		this.idCat = idCat;
	}

	public String getDetalleCat() {
		return this.detalleCat;
	}

	public void setDetalleCat(String detalleCat) {
		this.detalleCat = detalleCat;
	}

	public String getNombreCat() {
		return this.nombreCat;
	}

	public void setNombreCat(String nombreCat) {
		this.nombreCat = nombreCat;
	}

	public List<InvInventario> getInvInventarios() {
		return this.invInventarios;
	}

	public void setInvInventarios(List<InvInventario> invInventarios) {
		this.invInventarios = invInventarios;
	}

	public InvInventario addInvInventario(InvInventario invInventario) {
		getInvInventarios().add(invInventario);
		invInventario.setInvCategoria(this);

		return invInventario;
	}

	public InvInventario removeInvInventario(InvInventario invInventario) {
		getInvInventarios().remove(invInventario);
		invInventario.setInvCategoria(null);

		return invInventario;
	}

}