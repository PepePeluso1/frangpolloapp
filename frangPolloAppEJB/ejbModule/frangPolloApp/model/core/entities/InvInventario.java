package frangPolloApp.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the inv_inventario database table.
 * 
 */
@Entity
@Table(name="inv_inventario")
@NamedQuery(name="InvInventario.findAll", query="SELECT i FROM InvInventario i")
public class InvInventario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_inv", unique=true, nullable=false)
	private Integer idInv;

	@Column(name="cant_actual_inv", nullable=false)
	private Integer cantActualInv;

	@Column(name="cant_minima_inv", nullable=false)
	private Integer cantMinimaInv;

	@Column(name="nombre_inv", nullable=false, length=50)
	private String nombreInv;

	//bi-directional many-to-one association to InvCategoria
	@ManyToOne
	@JoinColumn(name="id_cat", nullable=false)
	private InvCategoria invCategoria;

	public InvInventario() {
	}

	public Integer getIdInv() {
		return this.idInv;
	}

	public void setIdInv(Integer idInv) {
		this.idInv = idInv;
	}

	public Integer getCantActualInv() {
		return this.cantActualInv;
	}

	public void setCantActualInv(Integer cantActualInv) {
		this.cantActualInv = cantActualInv;
	}

	public Integer getCantMinimaInv() {
		return this.cantMinimaInv;
	}

	public void setCantMinimaInv(Integer cantMinimaInv) {
		this.cantMinimaInv = cantMinimaInv;
	}

	public String getNombreInv() {
		return this.nombreInv;
	}

	public void setNombreInv(String nombreInv) {
		this.nombreInv = nombreInv;
	}

	public InvCategoria getInvCategoria() {
		return this.invCategoria;
	}

	public void setInvCategoria(InvCategoria invCategoria) {
		this.invCategoria = invCategoria;
	}

}