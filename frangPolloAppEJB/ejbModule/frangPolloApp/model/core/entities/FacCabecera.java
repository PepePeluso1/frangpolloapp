package frangPolloApp.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the fac_cabecera database table.
 * 
 */
@Entity
@Table(name="fac_cabecera")
@NamedQuery(name="FacCabecera.findAll", query="SELECT f FROM FacCabecera f")
public class FacCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="nro_factura", unique=true, nullable=false, length=17)
	private String nroFactura;

	
	@Column(name="fecha_factura", nullable=false)
	private Timestamp fechaFactura;

	@Column(name="porcentaje_iva_factura", nullable=false)
	private double porcentajeIvaFactura;

	@Column(name="subtotal_factura", nullable=false)
	private double subtotalFactura;

	@Column(name="total_factura", nullable=false)
	private double totalFactura;

	@Column(name="valor_iva_factura", nullable=false)
	private double valorIvaFactura;

	//bi-directional many-to-one association to CliCliente
	@ManyToOne
	@JoinColumn(name="cedula_cliente", nullable=false)
	private CliCliente cliCliente;

	//bi-directional many-to-one association to FacDetalle
	@OneToMany(mappedBy="facCabecera",cascade = CascadeType.ALL)
	private List<FacDetalle> facDetalles;

	public FacCabecera() {
	}

	public String getNroFactura() {
		return this.nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public Timestamp getFechaFactura() {
		return this.fechaFactura;
	}

	public void setFechaFactura(Timestamp fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public double getPorcentajeIvaFactura() {
		return this.porcentajeIvaFactura;
	}

	public void setPorcentajeIvaFactura(double porcentajeIvaFactura) {
		this.porcentajeIvaFactura = porcentajeIvaFactura;
	}

	public double getSubtotalFactura() {
		return this.subtotalFactura;
	}

	public void setSubtotalFactura(double subtotalFactura) {
		this.subtotalFactura = subtotalFactura;
	}

	public double getTotalFactura() {
		return this.totalFactura;
	}

	public void setTotalFactura(double totalFactura) {
		this.totalFactura = totalFactura;
	}

	public double getValorIvaFactura() {
		return this.valorIvaFactura;
	}

	public void setValorIvaFactura(double valorIvaFactura) {
		this.valorIvaFactura = valorIvaFactura;
	}

	public CliCliente getCliCliente() {
		return this.cliCliente;
	}

	public void setCliCliente(CliCliente cliCliente) {
		this.cliCliente = cliCliente;
	}

	public List<FacDetalle> getFacDetalles() {
		return this.facDetalles;
	}

	public void setFacDetalles(List<FacDetalle> facDetalles) {
		this.facDetalles = facDetalles;
	}

	public FacDetalle addFacDetalle(FacDetalle facDetalle) {
		getFacDetalles().add(facDetalle);
		facDetalle.setFacCabecera(this);

		return facDetalle;
	}

	public FacDetalle removeFacDetalle(FacDetalle facDetalle) {
		getFacDetalles().remove(facDetalle);
		facDetalle.setFacCabecera(null);

		return facDetalle;
	}

}