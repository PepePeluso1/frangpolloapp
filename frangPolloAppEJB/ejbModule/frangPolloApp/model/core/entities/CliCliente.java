package frangPolloApp.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cli_cliente database table.
 * 
 */
@Entity
@Table(name="cli_cliente")
@NamedQuery(name="CliCliente.findAll", query="SELECT c FROM CliCliente c")
public class CliCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cedula_cliente", unique=true, nullable=false, length=10)
	private String cedulaCliente;

	@Column(name="apellidos_cliente", nullable=false, length=100)
	private String apellidosCliente;

	@Column(name="correo_cliente", nullable=false, length=50)
	private String correoCliente;

	@Column(name="direccion_cliente", length=100)
	private String direccionCliente;

	@Column(name="nombres_cliente", nullable=false, length=100)
	private String nombresCliente;

	@Column(name="telefono_cliente", nullable=false, length=15)
	private String telefonoCliente;

	//bi-directional many-to-one association to CliUsuario
	@OneToMany(cascade = {CascadeType.MERGE}, mappedBy="cliCliente")
	private List<CliUsuario> cliUsuarios;

	//bi-directional many-to-one association to FacCabecera
	@OneToMany(cascade = {CascadeType.MERGE}, mappedBy="cliCliente")
	private List<FacCabecera> facCabeceras;

	public CliCliente() {
	}

	public String getCedulaCliente() {
		return this.cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}

	public String getApellidosCliente() {
		return this.apellidosCliente;
	}

	public void setApellidosCliente(String apellidosCliente) {
		this.apellidosCliente = apellidosCliente;
	}

	public String getCorreoCliente() {
		return this.correoCliente;
	}

	public void setCorreoCliente(String correoCliente) {
		this.correoCliente = correoCliente;
	}

	public String getDireccionCliente() {
		return this.direccionCliente;
	}

	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}

	public String getNombresCliente() {
		return this.nombresCliente;
	}

	public void setNombresCliente(String nombresCliente) {
		this.nombresCliente = nombresCliente;
	}

	public String getTelefonoCliente() {
		return this.telefonoCliente;
	}

	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}

	public List<CliUsuario> getCliUsuarios() {
		return this.cliUsuarios;
	}

	public void setCliUsuarios(List<CliUsuario> cliUsuarios) {
		this.cliUsuarios = cliUsuarios;
	}

	public CliUsuario addCliUsuario(CliUsuario cliUsuario) {
		getCliUsuarios().add(cliUsuario);
		cliUsuario.setCliCliente(this);

		return cliUsuario;
	}

	public CliUsuario removeCliUsuario(CliUsuario cliUsuario) {
		getCliUsuarios().remove(cliUsuario);
		cliUsuario.setCliCliente(null);

		return cliUsuario;
	}

	public List<FacCabecera> getFacCabeceras() {
		return this.facCabeceras;
	}

	public void setFacCabeceras(List<FacCabecera> facCabeceras) {
		this.facCabeceras = facCabeceras;
	}

	public FacCabecera addFacCabecera(FacCabecera facCabecera) {
		getFacCabeceras().add(facCabecera);
		facCabecera.setCliCliente(this);

		return facCabecera;
	}

	public FacCabecera removeFacCabecera(FacCabecera facCabecera) {
		getFacCabeceras().remove(facCabecera);
		facCabecera.setCliCliente(null);

		return facCabecera;
	}

}