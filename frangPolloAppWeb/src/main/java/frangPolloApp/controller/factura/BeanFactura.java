package frangPolloApp.controller.factura;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import frangPolloApp.controller.JSFUtil;
import frangPolloApp.model.cliente.managers.ManagerCliente;
import frangPolloApp.model.core.entities.CliCliente;
import frangPolloApp.model.core.entities.FacCabecera;
import frangPolloApp.model.core.entities.FacDetalle;
import frangPolloApp.model.core.entities.ProdProducto;
import frangPolloApp.model.factura.managers.ManagerFactura;

import java.util.Map;
import java.util.HashMap;
import java.sql.DriverManager;
import java.sql.Connection;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperExportManager;

@Named
@SessionScoped
public class BeanFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@EJB
	ManagerFactura mFactura;
	@EJB
	private ManagerCliente managerCliente;
	
	private List<FacCabecera> listaCabeceras;
	private List<FacDetalle> listaDetalles;
	private List<CliCliente> listaClientes;
	private List<ProdProducto> listaProductos;
	private FacCabecera CabeceraSeleccionada;
	private FacDetalle DetalleSeleccionado;
	private List<FacDetalle> DetallesSeleccionados;
	private List<FacDetalle> carrito;
	private boolean panelCabeceraColapsado;
	private boolean panelDetalleColapsado;
	private String Fecha;
	private CliCliente clienteNuevo;
	private CliCliente clienteSeleccionado;
	private ProdProducto productoSeleccionado;
	private int cantidad;
	
	
	
	@PostConstruct
	public void inicializar() {
		listaCabeceras = mFactura.findAllFacCabecera();
		listaDetalles = mFactura.findAllFacDetalle();
		listaClientes = mFactura.findAllCliCliente();
		listaProductos = mFactura.findAllProdProducto();
		CabeceraSeleccionada = new FacCabecera();
		DetalleSeleccionado = new FacDetalle();
		DetallesSeleccionados = new ArrayList<FacDetalle>();
		carrito = new ArrayList<FacDetalle>();
		clienteNuevo = new CliCliente();
		clienteSeleccionado = new CliCliente();
		CabeceraSeleccionada.setCliCliente(clienteSeleccionado);
		productoSeleccionado = new ProdProducto();
		panelDetalleColapsado = true;
		panelCabeceraColapsado = true;
		Fecha = "";
	}

	public void actioListenerAddCarrito(int idProducto, int cantidad){
		productoSeleccionado = mFactura.findProductoById(idProducto);
		DetalleSeleccionado.setProdProducto(productoSeleccionado);
		DetalleSeleccionado.setCantidadProd(cantidad);
		DetalleSeleccionado.setPrecioUnitProd(productoSeleccionado.getPrecioProd());
		DetalleSeleccionado.setTotalDetalleProd(cantidad*productoSeleccionado.getPrecioProd());
		carrito.add(DetalleSeleccionado);
		productoSeleccionado = new ProdProducto();
		DetalleSeleccionado = new FacDetalle();
		this.cantidad = 0;
		
	}
	
	public void actionListenerImprimirFactura(String nroFactura) {
		CabeceraSeleccionada = mFactura.findCabeceraByNro(nroFactura);
		DetallesSeleccionados = mFactura.findAllFacDetalleByNroFActura(nroFactura);
	}
	
	public void actionListenerSeleccionarCabecera(String nroFactura){
		CabeceraSeleccionada = new FacCabecera();
		CabeceraSeleccionada = mFactura.findCabeceraByNro(nroFactura);
		Fecha = CabeceraSeleccionada.getFechaFactura().toString();
		
	}
	
	public String actionListenerCarrito() {
		listaClientes = mFactura.findAllCliCliente();
		listaProductos = mFactura.findAllProdProducto();
		carrito = new ArrayList<FacDetalle>();
		clienteSeleccionado = new CliCliente();
		return "carrito";
	}
	
	public void seleccionarCliente(String cedula) {
		clienteSeleccionado = mFactura.findClienteBycedula(cedula);
	}
	
	public void generarFactura(String cedula, List<FacDetalle> car) {
		CabeceraSeleccionada = mFactura.generarFactura(cedula, car);
		DetallesSeleccionados = mFactura.findAllFacDetalleByNroFActura(CabeceraSeleccionada.getNroFactura());
		listaCabeceras = mFactura.findAllFacCabecera();
		listaDetalles = mFactura.findAllFacDetalle();
		carrito = new ArrayList<FacDetalle>();
		clienteSeleccionado = new CliCliente();
	}
	
	public void actionListenerActualizarCabecera() {
		try {
			System.out.println("Nueva fecha: " + Fecha);
			Timestamp stamp = Timestamp.valueOf(Fecha);
			System.out.println("Value of stamp: " + stamp);
			CabeceraSeleccionada.setFechaFactura(stamp);
			mFactura.actualizarCabecera(CabeceraSeleccionada);
			listaCabeceras = mFactura.findAllFacCabecera();
			JSFUtil.crearMensajeINFO("Datos actualizados");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerInsertarCliente() {
		try {
			managerCliente.insertarCliCliente(clienteNuevo);
			listaClientes = mFactura.findAllCliCliente();
			clienteNuevo = new CliCliente();
			JSFUtil.crearMensajeINFO("Datos de cliente insertados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String actionReporte(String numeroFactura){
		System.out.println("boton.");
		Map<String,Object> parametros=new HashMap<String,Object>();
        parametros.put("numeroFactura",numeroFactura);
        FacesContext context=FacesContext.getCurrentInstance();
        ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();
        String ruta=servletContext.getRealPath("factura/factura.jasper");
        System.out.println("ruta.");
        System.out.println(ruta);
        HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();
        response.addHeader("Content-disposition", "attachment;filename=factura.pdf");
        response.setContentType("application/pdf");
        try {
                Class.forName("org.postgresql.Driver");
                Connection connection = null;
                connection = DriverManager.getConnection(
                "jdbc:postgresql://192.100.198.141:5432/kaulcuangot","kaulcuangot", "1728220656");
                JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);
                JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
                context.getApplication().getStateManager().saveView ( context ) ;
                System.out.println("factura generada.");
                context.responseComplete();
        } catch (Exception e) {
                JSFUtil.crearMensajeERROR(e.getMessage());
                e.printStackTrace();
        }
        return "";
}
	
	public String getFecha() {
		return Fecha;
	}


	public void setFecha(String fecha) {
		Fecha = fecha;
	}


	public void acctionListenerEliminarCabecera(String nroFactura) {
		mFactura.eliminarCabecera(nroFactura);
		listaCabeceras = mFactura.findAllFacCabecera();
		JSFUtil.crearMensajeINFO("Cabecera eliminada");
	}
	
	public void actionListenerColapsarPanelCabecera() {
		panelCabeceraColapsado=!panelCabeceraColapsado;
	}
	public void actionListenerColapsarPanelDetalle() {
		panelDetalleColapsado=!panelDetalleColapsado;
	}

	public List<FacCabecera> getListaCabeceras() {
		return listaCabeceras;
	}


	public void setListaCabeceras(List<FacCabecera> listaCabeceras) {
		this.listaCabeceras = listaCabeceras;
	}


	public List<FacDetalle> getListaDetalles() {
		return listaDetalles;
	}


	public void setListaDetalles(List<FacDetalle> listaDetalles) {
		this.listaDetalles = listaDetalles;
	}


	public FacCabecera getCabeceraSeleccionada() {
		return CabeceraSeleccionada;
	}


	public void setCabeceraSeleccionada(FacCabecera cabeceraSeleccionada) {
		CabeceraSeleccionada = cabeceraSeleccionada;
	}


	public List<FacDetalle> getDetallesSeleccionados() {
		return DetallesSeleccionados;
	}


	public void setDetallesSeleccionados(List<FacDetalle> detallesSeleccionados) {
		DetallesSeleccionados = detallesSeleccionados;
	}


	public boolean isPanelCabeceraColapsado() {
		return panelCabeceraColapsado;
	}


	public void setPanelCabeceraColapsado(boolean panelCabeceraColapsado) {
		this.panelCabeceraColapsado = panelCabeceraColapsado;
	}


	public boolean isPanelDetalleColapsado() {
		return panelDetalleColapsado;
	}


	public void setPanelDetalleColapsado(boolean panelDetalleColapsado) {
		this.panelDetalleColapsado = panelDetalleColapsado;
	}


	public List<CliCliente> getListaClientes() {
		return listaClientes;
	}


	public void setListaClientes(List<CliCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}


	public CliCliente getClienteSeleccionado() {
		return clienteSeleccionado;
	}


	public void setClienteSeleccionado(CliCliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}


	public List<ProdProducto> getListaProductos() {
		return listaProductos;
	}


	public void setListaProductos(List<ProdProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}


	public int getCantidad() {
		return cantidad;
	}


	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public FacDetalle getDetalleSeleccionado() {
		return DetalleSeleccionado;
	}

	public void setDetalleSeleccionado(FacDetalle detalleSeleccionado) {
		DetalleSeleccionado = detalleSeleccionado;
	}

	public List<FacDetalle> getCarrito() {
		return carrito;
	}

	public void setCarrito(List<FacDetalle> carrito) {
		this.carrito = carrito;
	}

	public ProdProducto getProductoSeleccionado() {
		return productoSeleccionado;
	}

	public void setProductoSeleccionado(ProdProducto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}

	public CliCliente getClienteNuevo() {
		return clienteNuevo;
	}

	public void setClienteNuevo(CliCliente clienteNuevo) {
		this.clienteNuevo = clienteNuevo;
	}

	
	
}
