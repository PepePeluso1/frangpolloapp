package frangPolloApp.controller.productos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import frangPolloApp.controller.JSFUtil;
import frangPolloApp.model.core.entities.ProdCategoria;
import frangPolloApp.model.core.entities.ProdProducto;
import frangPolloApp.model.productos.managers.ManagerProductos;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanProductos implements Serializable {
	private static final long serialVersionUID=1L;
	
	@EJB
	private ManagerProductos managerProductos;
	private List<ProdProducto> listaProductos;
	private List<ProdCategoria> listaCategorias;
	private boolean panelColapsado;
	private ProdProducto producto;
	private ProdCategoria categoria;
	private ProdCategoria prodCategoria;
	private ProdProducto productoSeleccionado;
	private ProdCategoria categoriaSeleccionada;
	private int categoriaNueva;
	
	
	@PostConstruct
	public void inicializar(){
		listaProductos=managerProductos.findAllProdProducto();
		listaCategorias=managerProductos.findAllProdCategoria();
		prodCategoria=new ProdCategoria();
		producto= new ProdProducto();
		panelColapsado=true;
		
	}

	public void actionListenerColapsarPanel() {
		panelColapsado=!panelColapsado;
	}
	public void actionListenerInsertarProducto() {
		try {
			categoria=new ProdCategoria();
			categoria.setIdCatPro(categoriaNueva);
			producto.setProdCategoria(categoria);
			managerProductos.insertarProducto(producto);
			listaProductos=managerProductos.findAllProdProducto();
			producto= new ProdProducto();
			JSFUtil.crearMensajeINFO("Datos del producto Ingresados");
			}catch(Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	public void actionListenerEliminarProducto(int Id) {
		managerProductos.eliminarProducto(Id);
		listaProductos=managerProductos.findAllProdProducto();
		JSFUtil.crearMensajeINFO("Producto eliminado");
	}
	public void actionListenerSeleccionarProducto(ProdProducto producto) {
		productoSeleccionado=producto;
	}
	public void actionListenerActualizarProducto() {
		try {
			categoria=new ProdCategoria();
			categoria.setIdCatPro(categoriaNueva);
			productoSeleccionado.setProdCategoria(categoria);
			managerProductos.actualizarProducto(productoSeleccionado);
			listaProductos=managerProductos.findAllProdProducto();
			JSFUtil.crearMensajeINFO("Producto actualizado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerInsertarCategoria() {
		try {
			managerProductos.insertarCategoria(prodCategoria);
			listaCategorias=managerProductos.findAllProdCategoria();
			prodCategoria=new ProdCategoria();
			JSFUtil.crearMensajeINFO("Datos de la Categor�a Ingresados");
			}catch(Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	public void actionListenerEliminarCategoria(int Id) {
		managerProductos.eliminarCategoria(Id);
		listaCategorias=managerProductos.findAllProdCategoria();
		JSFUtil.crearMensajeINFO("Categor�a eliminada");
	}
	public void actionListenerSeleccionarCategoria(ProdCategoria categoriaSelec) {
		categoriaSeleccionada=categoriaSelec;
	}
	public void actionListenerActualizarCategoria() {
		try {
			managerProductos.actualizarCategoria(categoriaSeleccionada);
			listaCategorias=managerProductos.findAllProdCategoria();
			JSFUtil.crearMensajeINFO("Categor�a actualizada");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	public String actionReporte(){
        Map<String,Object> parametros=new HashMap<String,Object>();
        /*parametros.put("p_titulo_principal",p_titulo_principal);
        parametros.put("p_titulo",p_titulo);*/
        FacesContext context=FacesContext.getCurrentInstance();
        ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();
        String ruta=servletContext.getRealPath("productos/reporte_productos.jasper");
        System.out.println(ruta);
        HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();
        response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
        response.setContentType("application/pdf");
        try {
                Class.forName("org.postgresql.Driver");
                Connection connection = null;
                connection = DriverManager.getConnection(
                "jdbc:postgresql://192.100.198.141:5432/kaulcuangot","kaulcuangot", "1728220656");
                JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);
                JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
                context.getApplication().getStateManager().saveView ( context ) ;
                System.out.println("reporte generado.");
                context.responseComplete();
        } catch (Exception e) {
                JSFUtil.crearMensajeERROR(e.getMessage());
                e.printStackTrace();
        }
        return "";
	}
	
	public List<ProdProducto> getListaProductos() {
		return listaProductos;
	}
		
	public void setListaProductos(List<ProdProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public boolean isPanelColapsado() {
		return panelColapsado;
	}
	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}
	public ProdProducto getProducto() {
		return producto;
	}
	public void setProducto(ProdProducto producto) {
		this.producto = producto;
	}
	public ProdProducto getProductoSeleccionado() {
		return productoSeleccionado;
	}
	public void setProductoSeleccionado(ProdProducto productoSeleccionado) {
		this.productoSeleccionado = productoSeleccionado;
	}
	public List<ProdCategoria> getListaCategorias() {
		return listaCategorias;
	}
	public void setListaCategorias(List<ProdCategoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}
	public ProdCategoria getCategoria() {
		return categoria;
	}
	public void setCategoria(ProdCategoria categoria) {
		this.categoria = categoria;
	}
	public int getCategoriaNueva() {
		return categoriaNueva;
	}
	public void setCategoriaNueva(int categoriaNueva) {
		this.categoriaNueva = categoriaNueva;
	}
	public ProdCategoria getProdCategoria() {
		return prodCategoria;
	}
	public void setProdCategoria(ProdCategoria prodCategoria) {
		this.prodCategoria = prodCategoria;
	}
	public ProdCategoria getCategoriaSeleccionada() {
		return categoriaSeleccionada;
	}
	public void setCategoriaSeleccionada(ProdCategoria categoriaSeleccionada) {
		this.categoriaSeleccionada = categoriaSeleccionada;
	}
	
	
	


}
