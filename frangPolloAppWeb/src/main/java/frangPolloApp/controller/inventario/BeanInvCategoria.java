package frangPolloApp.controller.inventario;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import frangPolloApp.controller.JSFUtil;
import frangPolloApp.controller.seguridades.BeanSegLogin;
import frangPolloApp.model.core.entities.InvCategoria;
import frangPolloApp.model.inventario.managers.ManagerCategoriaInv;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanInvCategoria implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerCategoriaInv managerInvCategoria;
	private List<InvCategoria> listaInvCategoria;
	private InvCategoria nuevaCategoria;
	private InvCategoria edicionCategoria;
	
	@Inject
	private BeanSegLogin beanSegLogin;
	
	public BeanInvCategoria() {
		
	}
	
	@PostConstruct
	public void inicializar() {
		listaInvCategoria = managerInvCategoria.findAllInvCategoria();
	}
	
	public String actionMenuCategorias() {
		listaInvCategoria = managerInvCategoria.findAllInvCategoria();
		return "categorias";
	}
	
	public String actionMenuNuevaCategoria() {
		nuevaCategoria = new InvCategoria();
		return "categoria_nueva";
	}
	
	public String actionReporteCategoria() {
		Map<String,Object> parametros=new HashMap<String,Object>();
		 FacesContext context=FacesContext.getCurrentInstance();
		 ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();
		 String ruta=servletContext.getRealPath("inventario/inventario_reporte.jasper");
		 System.out.println(ruta);
		 HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();
		 response.addHeader("Content-disposition", "attachment;filename=reporte_categoria_inventario.pdf");
		 response.setContentType("application/pdf");
		 try {
		 Class.forName("org.postgresql.Driver");
		 Connection connection = null;
		 connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/kaulcuangot","kaulcuangot", "1728220656");
		 JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);
		 JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
		 context.getApplication().getStateManager().saveView( context );
		 System.out.println("reporte generado.");
		 context.responseComplete();
		 } catch (Exception e) {
		 JSFUtil.crearMensajeERROR(e.getMessage());
		 e.printStackTrace();
		 }		
		return "";
	}
	
	public void actionListenerInsertarNuevaCategoria() {
		try {
			managerInvCategoria.insertarCategoria(nuevaCategoria);
			listaInvCategoria=managerInvCategoria.findAllInvCategoria();
			nuevaCategoria=new InvCategoria();
			JSFUtil.crearMensajeINFO("Categoria insertada.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public String actionSeleccionarEdicionCategoria(InvCategoria categoria) {
		edicionCategoria=categoria;
		return "categoria_edicion";
	}
	
	public void actionListenerActualizarEdicionCategoria() {
		try {
			managerInvCategoria.actualizarCategoria(beanSegLogin.getLoginDTO(),edicionCategoria);
			listaInvCategoria=managerInvCategoria.findAllInvCategoria();
			JSFUtil.crearMensajeINFO("Categoria actualizada.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarCategoria(int idCat) {
		try {
			managerInvCategoria.eliminarCategoria(idCat);
			listaInvCategoria=managerInvCategoria.findAllInvCategoria();
			JSFUtil.crearMensajeINFO("Categoria eliminada.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<InvCategoria> getListaInvCategoria() {
		return listaInvCategoria;
	}

	public void setListaInvCategoria(List<InvCategoria> listaInvCategoria) {
		this.listaInvCategoria = listaInvCategoria;
	}

	public InvCategoria getNuevaCategoria() {
		return nuevaCategoria;
	}

	public void setNuevaCategoria(InvCategoria nuevaCategoria) {
		this.nuevaCategoria = nuevaCategoria;
	}

	public InvCategoria getEdicionCategoria() {
		return edicionCategoria;
	}

	public void setEdicionCategoria(InvCategoria edicionCategoria) {
		this.edicionCategoria = edicionCategoria;
	}

	
}
