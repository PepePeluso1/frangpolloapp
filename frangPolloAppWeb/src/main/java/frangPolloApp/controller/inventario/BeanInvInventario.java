package frangPolloApp.controller.inventario;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import frangPolloApp.controller.JSFUtil;
import frangPolloApp.model.core.entities.InvInventario;
import frangPolloApp.model.inventario.managers.ManagerInventario;

@Named
@SessionScoped
public class BeanInvInventario implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerInventario managerInventario;
	private List<InvInventario> listaInventario;
	private List<InvInventario> listaMinimaStock;
	private InvInventario nuevoInv;
	private InvInventario edicionInv;
	private int identificacionCategoriaSeleccionada;
	private int agregarProductos;
	private InvInventario aProducto;
	
	public BeanInvInventario() {
	}
	
	@PostConstruct
	public void inicializar() {
		nuevoInv = new InvInventario();
	}
	
	public void actionAgregarCantidadProducto() {
		try {
			managerInventario.actualizarInventarioCantidad(aProducto, agregarProductos);
			listaInventario=managerInventario.findAllInvInventario();
			listaMinimaStock = managerInventario.findMinStockInvInventario();
			JSFUtil.crearMensajeINFO("Inventario editado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public String actionCargarMenuInventario() {
		listaInventario = managerInventario.findAllInvInventario();
		return "inventario?faces-redirect=true";
	}
	
	public void actionCargarMinimoInventario() {
		listaMinimaStock = managerInventario.findMinStockInvInventario();
	}
	
	public String actionCargarMenuReporteMinimoStock() {
		actionCargarMinimoInventario();
		return "reporte_minimo_stock?faces-redirect=true";
	}
	
	
	
	
	public void actionListenerInsertarInventario() {
		try {
			managerInventario.insertarInventario(nuevoInv, identificacionCategoriaSeleccionada);
			listaInventario=managerInventario.findAllInvInventario();
			JSFUtil.crearMensajeINFO("Inventario creado.");
			nuevoInv=new InvInventario();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerCargarInventario(InvInventario inventario) {
		edicionInv=inventario;
	}
	public void actionListenerCargarInventarioMinimo(InvInventario inventariomin) {
		aProducto=inventariomin;
	}
	
	public void actionListenerGuardarEdicionInventario() {
		try {
			managerInventario.actualizarInventario(edicionInv, identificacionCategoriaSeleccionada);
			listaInventario=managerInventario.findAllInvInventario();
			JSFUtil.crearMensajeINFO("Inventario editado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
	public void actionListenerEliminarInventario(int idInv) {
		try {
			managerInventario.eliminarInventario(idInv);
			listaInventario=managerInventario.findAllInvInventario();
			JSFUtil.crearMensajeINFO("Inventario eliminado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public List<InvInventario> getListaInventario() {
		return listaInventario;
	}

	public void setListaInventario(List<InvInventario> listaInventario) {
		this.listaInventario = listaInventario;
	}

	public InvInventario getNuevoInv() {
		return nuevoInv;
	}

	public void setNuevoInv(InvInventario nuevoInv) {
		this.nuevoInv = nuevoInv;
	}

	public InvInventario getEdicionInv() {
		return edicionInv;
	}

	public void setEdicionInv(InvInventario edicionInv) {
		this.edicionInv = edicionInv;
	}

	public int getIdentificacionCategoriaSeleccionada() {
		return identificacionCategoriaSeleccionada;
	}

	public void setIdentificacionCategoriaSeleccionada(int identificacionCategoriaSeleccionada) {
		this.identificacionCategoriaSeleccionada = identificacionCategoriaSeleccionada;
	}

	public List<InvInventario> getListaMinimaStock() {
		return listaMinimaStock;
	}

	public void setListaMinimaStock(List<InvInventario> listaMinimaStock) {
		this.listaMinimaStock = listaMinimaStock;
	}

	public int getAgregarProductos() {
		return agregarProductos;
	}

	public void setAgregarProductos(int agregarProductos) {
		this.agregarProductos = agregarProductos;
	}

	public InvInventario getaProducto() {
		return aProducto;
	}

	public void setaProducto(InvInventario aProducto) {
		this.aProducto = aProducto;
	}

	
	
	
	

	
}
