package frangPolloApp.controller.cliemte;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import frangPolloApp.controller.JSFUtil;
import frangPolloApp.model.cliente.managers.ManagerCliente;
import frangPolloApp.model.core.entities.CliCliente;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanCliCliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerCliente managerCliente;
	private List<CliCliente> listadoClientes;
	private boolean panelColapsado;
	private CliCliente cliente;
	private CliCliente clienteSeleccionado;
	
	public BeanCliCliente() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void inicializar( ) {
		panelColapsado = true;
		cliente = new CliCliente();
	}
	
	public void actionListenerColapsarPanel() {
		panelColapsado=!panelColapsado;
	}
	
	public String actionCargarCliClientes() {
		listadoClientes = managerCliente.findAllCliClientes();
		return "clientes";
	}
	
	public void actionListenerInsertarCliente() {
		try {
			managerCliente.insertarCliCliente(cliente);
			listadoClientes = managerCliente.findAllCliClientes();
			cliente = new CliCliente();
			JSFUtil.crearMensajeINFO("Datos de cliente insertados.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerSeleccionarCliente(CliCliente cliente) {
		clienteSeleccionado = cliente;
	}
	
	public void actionListenerActualizarCliente() {
		try {
			managerCliente.actualizarCliCliente(clienteSeleccionado);
			listadoClientes = managerCliente.findAllCliClientes();
			cliente = new CliCliente();
			JSFUtil.crearMensajeINFO("Datos de cliente actualizados.");
		}catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarCliente(String cedula) {
		managerCliente.eliminarCliCliente(cedula);
		listadoClientes = managerCliente.findAllCliClientes();
		cliente = new CliCliente();
	}
	
	public String actionClienteReporte() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("cliente/clienteReport.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=clientesReporte.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/kaulcuangot", "kaulcuangot",
					"1728220656");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte de clientes generado.");
			context.responseComplete();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
		return "";
	}

	public List<CliCliente> getListadoClientes() {
		return listadoClientes;
	}

	public void setListadoClientes(List<CliCliente> listadoClientes) {
		this.listadoClientes = listadoClientes;
	}

	public boolean isPanelColapsado() {
		return panelColapsado;
	}

	public void setPanelColapsado(boolean panelColapsado) {
		this.panelColapsado = panelColapsado;
	}

	public CliCliente getCliente() {
		return cliente;
	}

	public void setCliente(CliCliente cliente) {
		this.cliente = cliente;
	}
	
	public CliCliente getClienteSeleccionado() {
		return clienteSeleccionado;
	}

	public void setClienteSeleccionado(CliCliente clienteSeleccionado) {
		this.clienteSeleccionado = clienteSeleccionado;
	}

}
